<!DOCTYPE html>
<html <?php language_attributes(); ?> >
  <head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>Ecovis KGA :: Accountancy + Business Acumen</title>
	<meta name="description" content="Ecovis KGA :: Accountancy + Business Acumen" />
	<meta name="keywords" content="Ecovis KGA, KGA, Chartered Accountants, Accountancy, Corporate Services" />
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/ccm.base.css?v=5bd4eb339f2fabd76354314ee298c0d6" />
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.js?v=5bd4eb339f2fabd76354314ee298c0d6"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/ccm.base.js?v=5bd4eb339f2fabd76354314ee298c0d6"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/buttons.css" rel="stylesheet" media="screen">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/flexslider.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/MyFontsWebfontsKit.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/faq.css" />
	<link href="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />
	
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap-transition.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap-collapse.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.scrollTo-1.4.3.1.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.scrollorama.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.easing.1.3.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.scrolldeck.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.ba-throttle-debounce.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider-min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/faq.js"></script>
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="//use.typekit.net/hgn5cnl.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<script>
	 	$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});
	</script>
	
    <?php wp_head(); ?>
  </head>
  <body data-spy="scroll" data-target=".navbar" >
  	<div class="container-fluid">
  		<div class="navbar navbar-inverse navbar-fixed-top">
  			<div class="navbar-inner">
  				<a class="brand" href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/Ecovis-KGA.png" alt="Ecovis KGA" /></a>
  					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
		        	</a>
		        	<div class="nav-collapse collapse">
						<ul class="nav nav-pills pull-right">
							<li>
								<a href="#slide1">Why Us?</a>
							</li>
							<li>
								<a href="#slide2">What We Do</a>
							</li>
							<li>
								<a href="#slide3">Our People</a>
							</li>
							<li>
								<a href="#slide5">Success Stories</a>
							</li>
							<li>
								<a href="#slide6">Contact</a>
							</li>
							<li>
								<a href="#slide7">Resources</a>
							</li>			
						</ul>
					</div>	
			</div>
		</div>

	 

  