<?php get_header(); ?>
     <?php if (have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>    
             
<div class="row-fluid">
	<div class="span12 slides-container">
		<div id="why-us-carousel" class="flexslider">
			<ul class="slides">
			  <li class="item active">
		    	<div class="slide" id="slide1">
					<div class="row">
					
					 <div class="span6">
					 </div>
						
					 <div class="span6">
						<h1>Welcome to<br>ECOVIS KGA</h1>
						<h2>ACCOUNTANCY + BUSINESS ACUMEN</h2>	
						<p>Our clients see us as like-minded business partners. Agile, personable and highly focused specialists with real world business expertise + 5 decades of collective consulting experience.</p>		

						<a href="#" data-featherlight="#mylightbox" class="button">Contact Us</a>
						<div id="mylightbox" class="sssh">
							<h3>Contact Us</h3>
							<p>We would love to help you out. Fill in the form below and we will be in touch with you shortly</p>
							<div class="two-thirds">
								<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
							</div>
							<div class="third">
								<p><strong>Phone:</strong></p> 
								<p>09 921 4630 <br />Fax: 09 379 5499</p>
								<p><strong>Email:</strong></p>
								<p><a href="mailto:admin@ecoviskga.co.nz">admin@ecoviskga.co.nz</a>
							</div>
						</div>	
					  </div>
					
					</div>
					
				</div>
			   </li>
			    <li class="item active">
		    	<div class="slide" id="slide11">
					<div class="row">
					
					 <div class="span6" style="padding-bottom: 5px;">
						 	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/xero.png" alt="Xero Gold Partners" />
					 </div>
						
					 <div class="span6" style="padding-bottom: 0px;">
						<h1>Our Heads are<br> in the Cloud</h1>
						<h2>CERTIFIED XERO GOLD PARTNER</h2>	
						<p>Xero integrates with 450+ other cloud based applications, making life in the cloud a breeze by tracking your financial data on a real time basis anywhere, anytime.</p>
						<p>They are leading the charge into cloud computing and we are going with them. </p>
						<p>Join us and move your business into the new era.</p>	

					  </div>

					</div>
					
				</div>
			   </li>
			</ul><!-- /.carousel-inner -->
		</div><!-- /.carousel -->


<div class="slide" id="slide2">
<div class="row">
	<div class="span12 what-we-do">
		<div class="span10">
			<h1>What We Do </h1>
		</div>
		<div id="what-we-do-carousel" class="flexslider span12">
			<ul class="slides">

				<li class="item1">
					<div class="span10">
						
						<!--<h2>Accountancy + Business Acumen</h2>-->
						<h2><?php the_field('overview'); ?></h2>
					</div>
					<div id="carousel-buttons"> 
						<ul>
							<li class="wLink1"><span>FINANCIAL REPORTING<br>&amp; TAX SERVICES</span></li>
							<li class="wLink2"><span>SPECIALIST SERVICES</span></li>
							<li class="wLink3"><span>DEVELOP YOUR BUSINESS</span></li>

						</ul>
					</div>
					<div class="span10">
						<p>We take an objective and immersive approach to working with our clients. Our expertise is valued across corporate advisory, general accounting services, specialist taxation advice, and wider business strategy.</p>
						<p>From our KGA office in Auckland, and through our international affiliation to ECOVIS, we help manage the business interests of individuals, SMEs and larger corporations throughout New Zealand, Australia and Asia.</p>
						<p>All of this is applied with relentless passion, insight and commitment.</p>	
						<a href="#slide6" class="button">Get in touch</a>				
					</div>
					
				</li>
				<li class="item2">
					<div class="span4">
						<ul class="subNav">
							<li class="wLink0"><p>1 |&nbsp;</p><p> OVERVIEW</p></li>
							<li class="wLink1 active"><p>2 |&nbsp;</p><p> FINANCIAL REPORTING<br>&amp; TAX SERVICES</p></li>
							<li class="wLink2"><p>3 |&nbsp;</p><p> SPECIALIST SERVICES</li>
							<li class="wLink3"><p>4 |&nbsp;</p><p> DEVELOP YOUR BUSINESS</li>

						</ul>
					</div>
					<div class="span8 full-size">
						<?php the_field('financial_copy'); ?>
						<a href="#" data-featherlight="#mylightbox" class="button">Enquire now</a>
					</div>
				</li>
			
				
				<li class="item3">
					<div class="span4">
						<ul class="subNav">
							<li class="wLink0"><p>1 |&nbsp;</p><p> OVERVIEW</p></li>
							<li class="wLink1"><p>2 |&nbsp;</p><p> FINANCIAL REPORTING<br>&amp; TAX SERVICES</p></li>
							<li class="wLink2 active"><p>3 |&nbsp;</p><p> SPECIALIST SERVICES</li>
							<li class="wLink3"><p>4 |&nbsp;</p><p> DEVELOP YOUR BUSINESS</li>
						</ul>
					</div>
					<div class="span8 full-size">
						<?php the_field('specialist_copy'); ?> 
												   
						<?php if( have_rows('specialist_accordion') ): ?>					
						<div class="cd-faq-items">
							<ul id="basics" class="cd-faq-group">
								<?php while ( have_rows('specialist_accordion') ) : the_row(); ?>

								<li>
									<a class="cd-faq-trigger" href="#0"><?php the_sub_field('topic_title'); ?></a>
									<div class="cd-faq-content">
										<?php the_sub_field('topic_content'); ?>
										<a href="#" data-featherlight="#mylightbox" class="button">Enquire now</a>
									</div> <!-- cd-faq-content -->
								</li>
								<?php endwhile; ?>
								
							</ul>
						</div>
						<?php else :
						// no rows found
						endif; ?>				
												
								
										
					</div>
				</li>
				<li class="item4">
					<div class="span4">
						<ul class="subNav">
							<li class="wLink0"><p>1 |&nbsp;</p><p> OVERVIEW</p></li>
							<li class="wLink1"><p>2 |&nbsp;</p><p> FINANCIAL REPORTING<br>&amp; TAX SERVICES</p></li>
							<li class="wLink2"><p>3 |&nbsp;</p><p> SPECIALIST SERVICES</li>
							<li class="wLink3 active"><p>4 |&nbsp;</p><p> DEVELOP YOUR BUSINESS</li>
						</ul>
					</div>
					<div class="span8 full-size">						
						<?php the_field('development_copy'); ?> 
												   
						<?php if( have_rows('development_accordion') ): ?>					
						<div class="cd-faq-items">
							<ul id="basics" class="cd-faq-group">
								<?php while ( have_rows('development_accordion') ) : the_row(); ?>

								<li>
									<a class="cd-faq-trigger" href="#0"><?php the_sub_field('topic_title'); ?></a>
									<div class="cd-faq-content">
										<?php the_sub_field('topic_content'); ?>
										<a href="#" data-featherlight="#mylightbox" class="button">Enquire now</a>
									</div> <!-- cd-faq-content -->
								</li>
								<?php endwhile; ?>
								
							</ul>
						</div>
						<?php else :
						// no rows found
						endif; ?>				
													
					</div>
				</li>
				
		
			</ul><!-- /.carousel-inner -->
		</div><!-- /.carousel -->

	</div>
</div>
</div>

<div class="slide" id="slide3">

	<div class="row">
		<div class="span4">
			<h1>Our People </h1>
			<h3><?php the_field('intro_copy'); ?> </h3>	
		</div>
		<div class="span4">
			<?php the_field('copy_col_one'); ?> 
		</div>
		
		<div class="span4">
			<?php the_field('copy_col_two'); ?> 	
			<a href="#" data-featherlight="#mylightbox" class="button">Book an appointment today</a>
			<div id="mylightbox" class="sssh">
				<h3>Contact Us</h3>
				<p>We would love to help you out. Fill in the form below and we will be in touch with you shortly</p>
				<div class="two-thirds">
					<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				</div>
				<div class="third">
					<p><strong>Phone:</strong></p> 
					<p>09 921 4630 <br />Fax: 09 379 5499</p>
					<p><strong>Email:</strong></p>
					<p><a href="mailto:admin@ecoviskga.co.nz">admin@ecoviskga.co.nz</a>
				</div>
			</div>	
		</div>
	</div>

</div>

<div class="slide" id="slide4">
	<div class="row">
		<ul class="thumbnails">
		
		
		<div class="span3 director vcard" id="S1">
			<a>
				<label class="SName mobile-hide">
				<span class="fn mobile-hide">Keith Goodall</span>
				</label>
				<span class="photo"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/keith-goodall.jpg" width="480" height="582" alt="Keith Goodall" /></span>
			</a>
			<div class="SInfo S1" >
					<p><span class="title role">Keith Goodall</span></p>
					<p><span class="title role">Director</span></p>
					<p class="mobile-hide">DDI: 09 921 4634</p>
					<a class="social mobile-show" href="tel: 09 921 4634"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>
					<!--<a class="social" target="_blank" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:keith.goodall@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p data-featherlight="#keith-goodall" style="width:100%; clear:both; padding-top: 20px; font-weight: 600;">Full Profile</p>
			</div>
			<div id="keith-goodall" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">KEITH GOODALL</span></strong></p>
					<p><strong><span class="title role">Director</span></strong></p>
				<p>Keith is a capable company director, holding numerous positions in New Zealand and Australia. Keith divides his time between directorships and providing expert witness opinions in matrimonial property valuations, shareholder disputes and economic loss quantification.</p><p><strong>Contact:</strong><br>DDI: 09 921 4634<br>Email: <a href="mailto:keith.goodall@ecoviskga.co.nz">keith.goodall@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
		<div class="span3 director vcard"  id="S2">
			<a>
				<label class="SName mobile-hide">
				<span class="fn mobile-hide">Wade Glass</span>
				</label>
				<span class="photo"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/Wade-Glass.jpg" width="480" height="582" alt="Wade Glass" /></span>
			</a>
			<div class="SInfo S2">
					<p><span class="title role">Wade Glass</span></p>
					<p><span class="title role">Director</span></p>
					<p class="mobile-hide">DDI: 09 921 4619</p>
					<a class="social mobile-show" href="tel: 09 921 4619"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>
					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:wade.glass@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#wade-glass">Full Profile</p>
			</div>
			<div id="wade-glass" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">WADE GLASS</span></strong></p>
					<p><strong><span class="title role">Director</span></strong></p>
				<p>Wade specialises in the areas of valuation, financial modelling and other transactional work. He advises a number of high net worth clients on various taxation and financial matters, including inbound and outbound cross-border investment structures.<br /><br />Wade owns and manages a forest ownership and harvesting business where he tries to practice what he preaches.</p><p><strong>Contact:</strong><br>DDI: 09 921 4619<br>Email: <a href="mailto:wade.glass@ecoviskga.co.nz">wade.glass@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
				
		</div>
		
		<div class="span3 director vcard" id="S3">
			<a>
				<label class="SName mobile-hide">
				<span class="fn mobile-hide">Clive Bish</span>
				</label>
				<span class="photo"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/Clive-Bish.jpg" width="480" height="582" alt="Clive Bish" /></span>
			</a>
			<div class="SInfo S3" >
					<p><span class="title role">Clive Bish</span></p>
					<p><span class="title role">Director</span></p>
					<p class="mobile-hide">DDI: 09 921 4655</p>
					<a class="social mobile-show" href="tel: 09 921 46455"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>
					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:clive.bish@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#clive">Full Profile</p>
			</div>
			<div id="clive" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">Clive Bish</span></strong></p>
					<p><strong><span class="title role">Director</span></strong></p>
				<p>Clive has gained experience in large accounting firms and the Serious Fraud Office. He advises a number of high net worth families and enjoys developing reporting and profit improvement strategies for his clients.</p><p><strong>Contact:</strong><br>DDI: 09 921 4655<br>Email: <a href="mailto:clive.bish@ecoviskga.co.nz">clive.bish@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
		<div class="span3 director vcard" id="S4">
			<a>
				<label class="SName mobile-hide">
				<span class="fn mobile-hide">Gareth Hoole</span>
				</label>
				<span class="photo"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/Gareth-Hoole.jpg" width="480" height="582" alt="Gareth-Hoole.jpg" /></span>
			</a>
			<div class="SInfo S4" >
					<p><span class="title role">Gareth Hoole</span></p>
					<p><span class="title role">Director</span></p>
					<p class="mobile-hide">DDI: 09 921 4637</p>
					<a class="social mobile-show" href="tel: 09 921 4637"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>

					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:gareth.hoole@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#gareth">Full Profile</p>
			</div>
			<div id="gareth" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">Gareth Hoole</span></strong></p>
					<p><strong><span class="title role">Director</span></strong></p>
				<p>Gareth Hoole is a Chartered Accountant with over 30 years of experience gained in New Zealand and abroad. He has a passion for adding value to businesses, particularly those that may face challenges and specialises in business recovery matters. He has a proven track record in providing pragmatic solutions to businesses in distress.</p><p><strong>Contact:</strong><br>DDI: 09 921 4637<br>Email: <a href="mailto:gareth.hoole@ecoviskga.co.nz">gareth.hoole@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
	

		<div class="span3 director vcard spacer-left" id="S6">
			<a>
				<label class="SName mobile-hide">
				<span class="fn mobile-hide">Carien Louw</span>
				</label>
				<span class="photo"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/carien-louw.jpg" width="480" height="582" alt="Carien Louw" /></span>
			</a>
			<div class="SInfo S6">
					<p><span class="title role">Carien Louw</span></p>
					<p><span class="title role">Senior  Associate</span></p>
					<p class="mobile-hide">DDI: 09 921 4635</p>
					<a class="social mobile-show" href="tel: 09 921 4635"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>

					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:carien.louw@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#carien">Full Profile</p>
			</div>
			<div id="carien" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">Carien Louw</span></strong></p>
					<p><strong><span class="title role">Senior  Associate</span></strong></p>
				<p>Carien has been with EcovisKGA  since 1999 and has over 30 years&rsquo; experience in all aspects of tax, business services,  forensic accounting,  cost accounting and auditing.  Carien works closely with our clients and provides sound advice around their accounting, banking and tax affairs.</p><p><strong>Contact:</strong><br>DDI: 09 921 4635<br>Email: <a href="mailto:carien.louw@ecoviskga.co.nz">carien.louw@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
		<div class="span3 director vcard" data-featherlight="#roger-bannan" id="S7">
			<a>
				<label class="SName mobile-hide">
					<span class="fn mobile-hide">Roger Bannan</span>
				</label>
				<span class="photo">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/roger-bugs.jpg" width="480" height="582" alt="Bernice Lo" />
				</span>
			</a>
			<div class="SInfo S7" >
					<p><span class="title role">Roger Bannan</span></p>
					<p><span class="title role">Principal</span></p>
					<p class="mobile-hide">DDI: 09 921 4641</p>
					<a class="social mobile-show" href="tel: 09 921 4641"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>

					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:roger.bannan@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#roger-bannan">Full Profile</p>
			</div>

			<div id="roger-bannan" class="sssh">
				
				<div>
					<p><strong><span class="title role">ROGER BANNAN</span></strong></p>
					<p><strong><span class="title role">Principal</span></strong></p>
					<p>Roger has a special interest in working with small to medium sized enterprises and advises on matters involving structure, management, reporting systems, financial planning and governance. He is also involved as a board member with several charities and community organisations.</p>
					<p><strong>Contact:</strong>
					<br>DDI: 09 921 4641
					<br>Email: <a href="mailto:roger.bannan@ecoviskga.co.nz">roger.bannan@ecoviskga.co.nz</a></p>
					<span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
			
		<div class="span3 director vcard" id="S5">
			<a>
				<label class="SName mobile-hide">
					<span class="fn mobile-hide">Bernice Lo</span>
				</label>
				<span class="photo">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Bernice-Lo.jpg" width="480" height="582" alt="Bernice Lo" />
				</span>
			</a>
			<div class="SInfo S5" >
					<p><span class="title role">Bernice Lo</span></p>
					<p><span class="title role">Associate &amp; General Manager of Asia Business</span></p>
					<p class="mobile-hide">DDI: 09 921 4635</p>
					<a class="social mobile-show" href="tel: 09 921 4635"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/phone.svg" </a></a>

					<!--<a class="social" href="http://linkedin.com"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/linkedin.svg" </a></a>-->
					<a class="social" href="mailto:bernice.lo@ecoviskga.co.nz"><img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/email.svg" </a></a></p>	
					<p style="width:100%; clear:both; padding-top: 20px; font-weight: 600;" data-featherlight="#bernice">Full Profile</p>
			</div>
			<div id="bernice" class="sssh">
				<!--<div class="two-thirds">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/133693532' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>-->
				<div>
					<p><strong><span class="title role">Bernice Lo</span></strong></p>
					<p><strong><span class="title role">Associate &amp; General Manager of Asia Business</span></strong></p>
				<p>Bernice has established a sound reputation for providing simple, straight-forward tax and business advice to her clients, both in New Zealand and overseas. Fluent in English, Mandarin and Cantonese, Bernice specialises in land and property related transactions, sale and purchase of businesses, business valuations and IRD audits.</p><p><strong>Contact:</strong><br>DDI: 09 921 4635<br>Email: <a href="mailto:bernice.lo@ecoviskga.co.nz">bernice.lo@ecoviskga.co.nz</a></p><span class="org">Ecovis KGA</span>
				</div>
			</div>
		</div>
		
		<div class="clear clicker">
			<a href="#" data-featherlight="#staffdetails" class="btn">Our Team</a>
			<div id="staffdetails" class="sssh">
				<h2>Our Team</h2>
				<div class="quarter">
					<h3>Aryanne Trankels</h3>
					<p>Business Services Manager</p>	
					<p>09 921 4656</p>
					<a href="mailto:aryanne.trankels@ecoviskga.co.nz">aryanne.trankels@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Beverley Ding</h3>
					<p>Business Services Manager</p>
					<p>09 921 4639</p>
					<a href="mailto:beverley.ding@ecoviskga.co.nz">beverley.ding@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Beverley Ding</h3>
					<p>Business Services Manager</p>
					<p>09 921 4639</p>
					<a href="mailto:beverley.ding@ecoviskga.co.nz">beverley.ding@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Bruce Eaddy</h3>
					<p>Business Manager / CFO</p>
					<p>09 921 4602</p>
					<a href="mailto:bruce.eaddy@ecoviskga.co.nz">bruce.eaddy@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Graeme O'Leary</h3>
					<p>Business Services Manager</p>
					<p>09 921 4642</p>
					<a href="mailto:graeme.oleary@ecoviskga.co.nz">graeme.oleary@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Hannah Crane</h3>
					<p>Business Services Accountant</p>
					<p>09 921 4631</p>
					<a href="mailto:hannah.crane@ecoviskga.co.nz">hannah.crane@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Josh Carter</h3>
					<p>Business Services Accountant</p>	
					<p>09 921 4643</p>
					<a href="mailto:josh.carter@ecoviskga.co.nz">josh.carter@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Kate Klein</h3>
					<p>Client Services Manager</p>
					<p>09 921 4625</p>
					<a href="mailto:kate.klein@ecoviskga.co.nz">kate.klein@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Lorinda Kearney</h3>
					<p>Client Services Assistant</p>
					<p>09 921 4630</p>
					<a href="mailto:lorinda.kearney@ecoviskga.co.nz">lorinda.kearney@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Melissa Guest</h3>
					<p>Business Services Manager</p>
					<p>09 921 4657</p>
					<a href="mailto:melissa.guest@ecoviskga.co.nz">melissa.guest@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Rachel Radford</h3>
					<p>Business Services Manager</p>
					<p>09 921 4636</p>
					<a href="mailto:rachel.radford@ecoviskga.co.nz">rachel.radford@ecoviskga.co.nz</a>
				</div>
				<div class="quarter">
					<h3>Willem Mandemaker</h3>
					<p>Business Services Manager</p>
					<p>09 921 4633</p>
					<a href="mailto:willem.mandemaker@ecoviskga.co.nz">willem.mandemaker@ecoviskga.co.nz</a>
				</div>
			</div>
		</div>
		
	</div>
</div>


<div class="slide" id="slide5">
	<div class="row">
		<div class="span12">
			<h1>Real World<br>Success Stories</h1>
			<h3><?php the_field('real_world_intro'); ?></h3>
		</div>
	</div>
	<div class="row">
		<div class="successStories">
									   
			<?php if( have_rows('real_world_accordion') ): ?>					
			<div class="cd-faq-items span12">
				<ul id="basics" class="cd-faq-group">
					<?php while ( have_rows('real_world_accordion') ) : the_row(); ?>

					<li>
						<a class="cd-faq-trigger" href="#0"><h3><?php the_sub_field('topic_title'); ?></h3></a>
						<div class="cd-faq-content">
							<?php the_sub_field('topic_content'); ?>
						</div> <!-- cd-faq-content -->
					</li>
					<?php endwhile; ?>
					
				</ul>
			</div>
			<?php else :
			// no rows found
			endif; ?>				

		</div>
	</div>
</div>
</div>
<div class="slide" id="slide6">
	<h1>Contact</h1>
	<h3>
	<div class="half">
		<img style="float: left; margin-right: 40px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/newmarket.png" alt="newmarket.png" width="100%" />
	</div>
	<div class="quarter">
		<h3>Auckland</h3>
		
		<p><strong>Postal Address:</strong></p>
		<p>PO Box 37223 <br />Parnell <br />Auckland 1151 <br />New Zealand</p>
		<p><strong>Physical Address:</p> 
		<p>Level 2 Bupa House <br />5 - 7 Kingdon Street <br />Newmarket <br />Auckland 1023 <br />New Zealand</p>
	
		
	</div>
	<div class="quarter">
		<h3></h3>
		<p><strong>Phone:</strong></p> 
		<p>09 921 4630 <br />Fax: 09 379 5499</p>
		<p><strong>Email:</strong></p>
		<p><a href="mailto:admin@ecoviskga.co.nz">admin@ecoviskga.co.nz</a>		
	</div>
</div>
<div class="slide" id="slide7">
	<div class="row">
		<h1 id="resourcesHd">Resources</h1>
		<div class="span3 rTags">
			<ul id="tag-list">
				<!--<li><a class="Trusts" hef="#">Trusts</a></li>-->
				<li><a class="Questionnaires" hef="#">Questionnaires</a></li>
				<!--<li><a class="Income" hef="#">Income</a></li>
				<li><a class="Individuals" hef="#">Individuals</a></li>
				<li><a class="Businesses" hef="#">Businesses</a></li>
				<li><a class="Companies" hef="#">Companies</a></li>-->
			</ul>
		</div>
	
		<div class="span9 resourcesCont">
			<h3>Displaying: All</h3>
			<table>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Trust Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/21/1/" class="btn">Download</a></td>
			</tr>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Trust Administration Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/22/1/" class="btn">Download</a></td>
			</tr>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Rental Income Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/23/1/" class="btn">Download</a></td>
			</tr>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Individual Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/24/1/" class="btn">Download</a></td>
			</tr>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Business Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/25/1/" class="btn">Download</a></td>
			</tr>
				<tr class="Questionnaires">

				<td id="resourcesMiddle">2015 Company Administration Questionnaire</td>
				<td id="resourcesRight"><a href="/index.php/download_file/26/1/" class="btn">Download</a></td>
			</tr>
			</table>
		</div>
	
	</div>
</div>


               <?php endwhile; ?>
     <?php endif; ?>
<?php get_footer(); ?>