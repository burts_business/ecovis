$(window).load(function() {
	var deck = new $.scrolldeck({
		buttons: '.nav li a' 
	});
});

function refreshScrollSpy () {
	$('[data-spy="scroll"]').each(function () {
		$(this).scrollspy('refresh');
	});
}

$(window).resize($.throttle(250, refreshScrollSpy));

$(window).load(function() {
	$('#what-we-do-carousel').flexslider({
		slideshow: false,
		animationLoop: false,
		smoothHeight: true,
		controlNav: false
	});
	$('.wLink0').click(function () {
	    $('#what-we-do-carousel').flexslider(0);
	});
	$('.wLink1').click(function () {
	    $('#what-we-do-carousel').flexslider(1);
	});
	$('.wLink2').click(function () {
	    $('#what-we-do-carousel').flexslider(2);
	});
	$('.wLink3').click(function () {
	    $('#what-we-do-carousel').flexslider(3);
	});
	$('.wLink4').click(function () {
	    $('#what-we-do-carousel').flexslider(4);
	});
	$('#why-us-carousel').flexslider({
		slideshow: true,
		animationLoop: true,
		slideshowSpeed: 5000,
		
		controlNav: true
	});
	
	
	$('#testimonials-carousel').flexslider({
		slideshow: true,
		animationLoop: true,
		controlNav: false
	});
	$('.successStories').flexslider({
		slideshow: false,
		animationLoop: false,
		controlNav: true,
		manualControls: "waah",
  	});
  	hold_detach = jQuery('.successStories .flex-control-nav').detach();
  	jQuery('.successStories').prepend(hold_detach);
});

$(function () {
    $(".thumbnails a").popover({
        placement : 'top',
        html : true
    });
    $('.thumbnails a').on('click', function (e) {
        $('.thumbnails a').not(this).popover('hide');
    });
	$( ".thumbnails .director" ).hover(
	  function() {
	    var clickedItem = $(this).attr('id');
    	$('.SInfo.'+ clickedItem).fadeIn('slow');
	  }, function() {
	    $('.SInfo').hide();
	  }
	);
	$('.thumbnails .SClose, .SInfo').click(function(e){ 
		$('.SInfo').fadeOut('slow');
	});
});

$(function(){ 
     $('.nav').click( function() {
        $('.btn-navbar').addClass('collapsed');
        $('.nav-collapse').removeClass('in').css('height', '0');
    });
 });

$(document).ready(function() {

$("#tag-list").prepend('<li><a href="#" class="active">All</a></li>');
//remove duplicates
var seen = {};
$('#tag-list li a').each(function() {
    var txt = $(this).text();
    if (seen[txt])
        $(this).remove();
    else
        seen[txt] = true;
});
 
// filter the list when the filter links are clicked
$('#tag-list li a').live('click',function(e){
 
    // allows filter categories using multiple words
    $('.resourcesCont h3').text('Displaying: '+$(this).text());
    var getText = $(this).text().replace(" ", "-");
    if(getText == 'all'){
        $(".resourcesCont tr").fadeIn();
    } else {
        $(".resourcesCont tr").fadeOut();
        $(".resourcesCont tr."+getText).fadeIn();
    }
    // add class "active" to current filter item
    $('#tag-list li a').removeClass('active');
    $(this).addClass('active');
 
    // prevent the page scrolling to the top of the screen
    e.preventDefault();
});
});
 
// Function to create a distinct list from array
function distinctList(inputArray){
    var i;
    var length = inputArray.length;
    var outputArray = [];
    var temp = {};
    for (i = 0; i < length; i++) {
        temp[inputArray[i]] = 0;
    }
    for (i in temp) {
        outputArray.push(i);
    }
    return outputArray;
}