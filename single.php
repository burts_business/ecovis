<?php get_header(); ?>
<div id="single-posts">
	<div class="clear">
		     <?php if (have_posts()) : ?>
		               <?php while (have_posts()) : the_post(); ?>  
		               
		               	<div class="post-container">  
			               
			               <div class="post-left third">	
				               	<h3>Archives</h3>
				                <?php $args = array('type' => 'postbypost');
					          wp_get_archives ($args); ?>							
			               </div>
			               <div class="post-right two-thirds">
				              <h1><?php the_title();?></h1>
				               	
				               <?php
           						// check if the repeater field has rows of data
           						if( have_rows('article') ):
           						
           						 	// loop through the rows of data
           						    while ( have_rows('article') ) : the_row();?>
		   								<div id="<?php the_sub_field('article_slug');?>">
	           						        <h2><?php the_sub_field('article_title');?></h2>
	           						        <img src="<?php the_sub_field('image');?>" alt="Article Image" />
	           						        <p><?php the_sub_field('article_copy');?></p>
		   								</div>
           						        
           						
           						    <?php endwhile;
           						
           						else :
           						
           						    // no rows found
           						
           						endif;	
           					?>

				               
			               </div>			               
		               	</div>
		               <?php endwhile; ?>
		     <?php endif; ?>
	</div>
</div>	
		
<?php get_footer('single'); ?>